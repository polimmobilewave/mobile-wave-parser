const fs = require('fs');
const N3 = require('n3');
const N3Util = N3.Util;
const program = require('commander');
const parse = require('pg-query-native').parse;

const debug = console.log;

const start = function() {

  // Parsing the script argument
  program
    .option('-i, --input [path]', 'Path to obda input files')
    .option('-o, --output [path]', 'Path to MobileWave mapping output files')
    .option('-m, --mode [mode]', 'Source mode(json|sql)', /^(sql|json)$/i, 'json')
    .parse(process.argv);
  // Check if required options are provided
  if (!program.input) {
    console.error("No input file was given!");
    return;
  }
  if (!program.output) {
    console.warn("No output file was given!");
    program.output = program.input + ".json";
  }
  if (!program.mode) {
    console.warn("No mode was given!");
    program.mode = "json";
  }
  debug("Stating to parse: ", program.input);
  debug("Writing mappings in: ", program.output);
  debug("Operating in " + program.mode + " mode");

  fs.access(program.input, fs.constants.R_OK, function(err) {
    if (err) { // Unable to access the input file
      debug("Unable to access the input file: ", program.input);
    } else {
      // File can be read
      const parseContext = function(prefixeString) {
        // Function used to parse the prefix sections to create the context
        // of the mappings
        let context = {};
        let prefixList = prefixeString.split('\n');
        prefixList.forEach(function(item) {
          let prefix = item.replace(/\t+/g, ' ').split(": ", 2);
          if (prefix[0] && prefix[1])
            context[prefix[0].trim()] = prefix[1].trim();
        });
        return context;
      }

      const decode = function(item) {
        // Function used to replace every _O/CL_ to curly bracket
        return item.replace(/_OL_/g, '{').replace(/_CL_/g, '}');
      };
      const encode = function(item) {
        // Function used to replace every curly bracket to _O/CL_
        return item.replace(/\{/g, '_OL_').replace(/}/g, '_CL_');
      };




      const content = fs.readFileSync(program.input, 'utf8');
      debug("Read input file: ", program.input);

      const ptrnPrefix = '[PrefixDeclaration]';
      const ptrnSource = '[SourceDeclaration]';
      const ptrnMapping = '[MappingDeclaration]';
      // Split the file by different sections
      let idxPrefix = content.indexOf(ptrnPrefix);
      let idxSource = content.indexOf(ptrnSource);
      let idxMapping = content.indexOf(ptrnMapping);
      let jsonT = {
        "@context": {},
        "iGraph": [],
        "dGraph": [],
      };

      let prefixes = content.substr(idxPrefix + ptrnPrefix.length, idxSource - (idxPrefix + ptrnPrefix.length)).trim();
      let mappings = content.substr(idxMapping + ptrnMapping.length).trim();

      const parseMapping = function(mappings) {

        const adjustMapping = function(mappingString) {
          let _jMappings = mappingString.replace(/"/g, '\\\"')
            .replace(/\{/g, '{{')
            .replace(/}/g, '}}')
            .replace('@collection [[', '[{"dummy":"dummy')
            .replace(/mappingId/g, '"},{"mappingId":"')
            .replace(/target/g, '","target":"')
            .replace(/source/g, '","source":"')
            .replace(']]', '"}]')
            .replace(/\n/g, ' ')
            .replace(/\r/, ' ')
            .replace(/\t/g, '');

          return JSON.parse(_jMappings);
        }
        const parseSource = function(source) {
          const extractTables = function(list) {
            let tNames = [];
            list.forEach(function(table) {
              if (table["RangeVar"]) {
                let tName = table["RangeVar"]["relname"] ? table["RangeVar"]["relname"] : null;
                tNames.push(tName);
              } else if (table["JoinExpr"]) {
                tNames = tNames.concat(extractTables([table["JoinExpr"]["larg"], table["JoinExpr"]["rarg"]]));
              }
            });
            return tNames;
          };
          let ast = parse(source);
          let fromClause = ast["query"][0]["SelectStmt"]["fromClause"];
          return extractTables(fromClause);
        }
        const parseTarget = function(mappingId, target, context, sources) {
          let mappingKey = "";
          let _rdf = [];
          _rdf.push("@prefix : <#> .");
          for (var key in context) {
            if (context.hasOwnProperty(key)) {
              _rdf.push("@prefix " + key + ": <" + context[key] + "> .");
            }
          }
          _rdf.push(target);
          _rdf = _rdf.join("\n");
          let rdf = encode(_rdf);
          let parser = N3.Parser();
          let triples = parser.parse(rdf);
          let maps = {};
          if (triples) {
            triples.forEach(function(triple) {
              if (triple) {
                triple.subject = decode(triple.subject);
                triple.object = decode(triple.object);
                let isLiteral = false;
                if (N3Util.isLiteral(triple.object)) {
                  triple.object = N3Util.getLiteralValue(triple.object);
                  isLiteral = true;
                }


                mappingKey = mappingId + "_" + triple.subject;
                let mapping = maps[mappingKey] ? maps[mappingKey] : {
                  "subject": triple.subject,
                  "properties": [],
                  "sources": sources,
                };
                mapping["properties"].push({
                  "predicate": triple.predicate,
                  "object": triple.object,
                  "isLiteral": isLiteral,
                });
                maps[mappingKey] = mapping;
              }

            });
          }
          return maps[mappingKey];
        }

        let jMappings = adjustMapping(mappings);
        let maps = [];
        jMappings.forEach(function(item) {
          if (item["target"]) {
            debug('Parsing ', item['mappingId']);
            let mappingId = item['mappingId'];
            let sources = program.mode === "sql" ? item['source'] : parseSource(item['source']);
            maps.push(parseTarget(mappingId, item["target"], jsonT["@context"], sources));

          }
        });
        return maps;
      }

      jsonT["@context"] = parseContext(prefixes);
      debug("Context Generated!");
      jsonT["iGraph"] = parseMapping(mappings);
      debug("Mappings Generated!")
      fs.writeFileSync(program.output, JSON.stringify(jsonT, null, 2))
    }

  });
  // debug(JSON.stringify(jsonT["@context"], null, 2));
}
start();
// {
//   subject:   'http://example.org/cartoons#Tom',
//   predicate: 'http://example.org/cartoons#name',
//   object:    '"Tom"'
// }
