const fs = require('fs');
const $rdf = require('rdflib')
const parse = require('pg-query-native').parse;


const log = console.log

let content = fs.readFileSync('./data/obdaFile.obda', 'utf8');

let ptrnPrefix = '[PrefixDeclaration]';
let ptrnSource = '[SourceDeclaration]';
let ptrnMapping = '[MappingDeclaration]';

let idxPrefix = content.indexOf(ptrnPrefix);
let idxSource = content.indexOf(ptrnSource);
let idxMapping = content.indexOf(ptrnMapping);
let jsonT = {
  "@context": {},
};

let prefixes = content.substr(idxPrefix + ptrnPrefix.length, idxSource - (idxPrefix + ptrnPrefix.length)).trim();
let sources = content.substr(idxSource + ptrnSource.length, idxMapping - (idxSource + ptrnSource.length)).trim();
let mappings = content.substr(idxMapping + ptrnMapping.length).trim();
mappings = mappings.replace(/"/g, '\\\"')
  .replace(/\{/g, '\{\{')
  .replace(/\}/g, '\}\}')
  .replace('@collection [[', '[{"dummy":"dummy')
  .replace(/mappingId/g, '"},{"mappingId":"')
  .replace(/target/g, '","target":"')
  .replace(/source/g, '","source":"')
  .replace(']]', '"}]')
  .replace(/\n/g, ' ')
  .replace(/\r/, ' ')
  .replace(/\t/g, '');
mappings = JSON.parse(mappings);


prefixes = prefixes.split('\n');
prefixes.forEach(function (item) {
  let prefix = item.replace(/\t+/g, ' ')
    .split(": ", 2);
  if (prefix[0] && prefix[1])
    jsonT["@context"][prefix[0]] = prefix[1];
});

let jsonTmapping = {};
mappings.forEach(function (item) {
  if (item["target"]) {
    let tNames = [];
    let extractTables = function (list) {
      list.forEach(function (table, idx) {
        if (table["RangeVar"]) {
          let tName = table["RangeVar"]["relname"] ? table["RangeVar"]["relname"] : null;
          tNames.push(tName)
        } else if (table["JoinExpr"]) {
          return extractTables([table["JoinExpr"]["larg"], table["JoinExpr"]["rarg"]]);
        }
      });
    };
    let ast = parse(item["source"]);
    let tables = ast.query[0].SelectStmt.fromClause;
    extractTables(tables)

    let targets = item["target"].split('.');

    // log(JSON.stringify(ast, null, 4));
    targets.forEach(function (target) {
        let subTargets = target.split(';');
        let mapping = {};
        let properties = [];


        subTargets.forEach(function (subTarget) {
          let statement = subTarget.trim().split(' ');
          if (statement.length === 3) {
            mapping['target'] = statement[0];
            statement.shift();
          }

          if (statement.includes('a')) {
            let type = statement[1];
            if (jsonTmapping[type]) {
              mapping = jsonTmapping[type];
            } else {
              mapping['mapping'] = statement[1];
              mapping['source'] = tNames;
              jsonTmapping[type] = mapping;
            }
          } else if (statement.length === 2) {
            let pMapping = {};
            pMapping['mapping'] = statement[0];
            pMapping['target'] = statement[1];
            properties.push(pMapping);
          }

        });
        if (properties.length > 0) {
          mapping['properties'] = Array.isArray(mapping['properties']) ? mapping['properties'].concat(properties) : properties;
        }
      }
    );
  }
});
log(JSON.stringify(jsonTmapping, null, 4));

// TODO: if no type use rdf:Resource

// log(jsonT);
// log('<><><><><><>');
// log(sources);
// log('<><><><><><>');
// log(JSON.stringify(mappings, null, 2));
